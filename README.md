# AA Relays

This is an Alliance Auth App for forwarding, collating and filtering of messages from various chat services to defined outputs including Database logging.

![License](https://img.shields.io/badge/license-MIT-green) ![python](https://img.shields.io/badge/python-3.6-informational) ![django](https://img.shields.io/badge/django-3.1-informational)

## Features

- Sources
  - Discord Messages, Embeds

- DB Logging

- Destinations
  - Discord Webhook
  - Discord Channel Message via AA-DiscordBot

- UI (Well... admin interface) for selecting sources and destinations

- Filtering By

  - Server Source
  - Channel Source
  - Mentions AND/OR non mentions
  - Regex on the Message Content

- Web UI
  - Servers, their status and metrics
  - Messages in a server.

## Planned Features

- Discord Events, Threads, PrivateMessages
- Slack Source

## Installation

### Step One - Install

Install the app with your venv active

```shell
pip install git+https://gitlab.com/tactical-supremacy/aa-relays.git
```

Pull the Runners

```shell
wget https://gitlab.com/tactical-supremacy/aa-relays/-/raw/master/relays/runner_discord.py
```

### Step Two - Configure

- Add the following to `INSTALLED_APPS`

```python
'relays',
'ninja',
"ninja_apikey",
```

- Add the following to `urls.py`

```python
from relays.api import api as relays_api
urlpatterns = [
    # THERE WILL BE OTHER THINGS HERE, ADD THE BELOW LINE
    path('relays/api/', relays_api.urls),
]
```

- Add the below lines to your `local.py` settings file

```python
## Settings for AA-Relays ##
LOGGING['handlers']['relays_log_file']= {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_DIR, 'log/relays.log'),
            'formatter': 'verbose',
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 5,
        }
LOGGING['loggers']['relays'] = {'handlers': ['relays_log_file'],'level': 'DEBUG'}
CELERYBEAT_SCHEDULE['relays_update_statistics'] = {
    'task': 'relays.tasks.update_statistics',
    'schedule': crontab(minute='0', hour='*'),
}
CELERYBEAT_SCHEDULE['ping_all_runners'] = {
    'task': 'relays.tasks.ping_all_runners',
    'schedule': crontab(minute='*/5', hour='*'),
}
```

### Step Three - Update Project

- Run migrations `python manage.py migrate`
- Gather your staticfiles `python manage.py collectstatic`

### Step Four-A - Run Relays with Docker

The "Runners" need to be ran on your server separately for this to function. While they have the context of Django and Alliance Auth, Each "Relay" own runner process to operate.

If you are using Docker for Alliance Auth. Relay can spin up and down its own runners on demand, Alternatively use the supervisor config below.

Update your AA base container to have the runnerfile mapped and enable a connection to the Docker socket

```dockerfile
x-allianceauth-base: &allianceauth-base
  ...
  volumes:
  ...
    - ./conf/runner_discord.py:/home/allianceauth/myauth/runner_discord.py
    - /var/run/docker.sock:/var/run/docker.sock
  ...
```

check what group ID your Docker group is

```bash
getent group docker
```

Modify your custom.dockerfile to give your containers privileges to manage Docker, substuting 1001 with the result of the above command

```dockerfile
  ...
  USER root
  RUN groupadd -g 1001 docker && usermod -aG docker "${AUTH_USER}"
  USER ${AUTH_USER}
```

Add the full path to your docker-compose.yml file to `local.py`

```python
RELAYS_DOCKERCOMPOSE_DIRECTORY = "/opt/aa-docker/"
```

You can now Start/Stop/Manage containers via  /admin/relays/runnercontainer/

### Step Four-B - Run Relays with Supervisor

Supervisor is one option, which you may have for allianceauth already, this is a sample configuration for starting a runner for discord for the first AccessToken in the database. You would create a number of these entries for each token PK you have,

`/etc/supervisord.d/aarelays.conf` or `/etc/supervisord.d/aarelays.ini` depending on OS

```python
[program:runner_1]
command=/home/allianceserver/venv/auth/bin/python runner_discord.py 1
directory=/home/allianceserver/myauth/
user=allianceserver
stdout_logfile=/home/allianceserver/myauth/log/relays.log
stderr_logfile=/home/allianceserver/myauth/log/relays.log
autostart=true
autorestart=true
startsecs=10
priority=900

[group:aarelays]
programs=runner_1
priority=900
```

### Step Five - Enable Translations

I have moved away from public translation sources, the APIs are unreliable and break often. LibreTranslate can be self hosted <https://hub.docker.com/r/libretranslate/libretranslate> and they offer a flat rate unlimited use api for $ if you are lazy.

Install it / Add it to your docker-compose / Buy an API key

Add the following lines to your local.py (if you havent already for allianceauth-translate-tool)

```python
AA_TRANSLATIONS_URL = ""
AA_TRANSLATIONS_API_KEY = ""
AARELAYS_TRANSLATION_LANGUAGE = "en"  # https://github.com/argosopentech/argos-translate/blob/master/argostranslate/languages.csv
```

## Settings

| Name                          | Description                                                          | Default |
| ----------------------------- | -------------------------------------------------------------------- | ------- |
| RELAYS_TRANSLATION_LANGUAGE | When attempting a relay translation, what language do I translate to | "en" |
| AA_TRANSLATIONS_URL | FROM ALLIANCEAUTH-TRANSLATE-TOOL | "" |
| AA_TRANSLATIONS_API_KEY | FROM ALLIANCEAUTH-TRANSLATE-TOOL | None |
| RELAYS_TASK_PRIORITY_PROCESSMESSAGE | Celery Priority for Processing an Incoming Message | 1 |
| RELAYS_TASK_PRIORITY_SENDMESSAGE | Celery Priority for Processing an Incoming Message | 1 |
| RELAYS_TASK_PRIORITY_PROCESSEMBED | Celery Priority for Processing an Incoming Message | 2 |
| RELAYS_TASK_PRIORITY_SENDEMBED | Celery Priority for Processing an Incoming Message | 2 |
| RELAYS_APP_BRANDNAME | Branding | "AA Relays" |
| RELAYS_DOCKERCOMPOSE_DIRECTORY | Location of the Docker Compose file | "/opt/aa-docker/" |

## Permissions

| Name                | Purpose                                                | Code                  |
| ------------------- | ------------------------------------------------------ | --------------------- |
| Can Access This App | Allow users to submit Access Tokens from the Front-End | `relays.basic_access` |
| Can access this app and view relay messages | Can access this app and view relay messages | `relays.advanced_access` |
| Can submit messages via the API| Can submit messages via the API | `relays.api_submit` |

## Logic

For every message, looping through each Relay Configuration, Messages are Relayed based on the following logic order

```pseudo
Source Server Matches OR All Servers True/False
AND
Source Channel matches OR All Channels True/False
AND
@here/@everyone True/False OR Non Mentions True/False OR Regex (Default ".^" To not match anything, further below)
```

## Regex

Sometimes distinguishing between Mentions and Chatter isn't enough.

Theoretically the full regex library is supported here, but minimal testing has been done, ymmv.

AA Relays Adds header fields into the message string so these cam be regex-ed upon, like so.

`joined_content_with_headers = f"{message.channel.guild.name}/{message.channel.name}/{message.author}: {joined_content}"`

Examples

```psuedo
*supers*
*red pen*

*<@318309023478972417>* For User mentions
*<@&735881663799623710>* for Role Mentions
*supercarriers/318309023478972417/* For a Channel message by a specific Author
```

## Meta

```psuedocode
Runner ->
selfcord.on_message() event ->
relays.ingest.ingest_discord_message_new() ->
django.object ->
signal ->
queue up process_message()
<---thread--->
process_message() ->
fire webhooks / queue up discordbot tasks
<---thread--->
discordbot task ->
send discordbot message
~~fin~~
```
