#define PURPLE_PLUGINS

#include <glib.h>
#include <curl/curl.h>

#include <libpurple/account.h>
#include <libpurple/util.h>

#include "debug.h"
#include "notify.h"
#include "plugin.h"
#include "prefs.h"
#include "version.h"

#define PLUGIN_ID "aa-relays"

/* UI */
static PurplePluginPrefFrame *plugin_config_frame(PurplePlugin *plugin)
{
    PurplePluginPrefFrame *frame;
    PurplePluginPref *ppref;

    frame = purple_plugin_pref_frame_new();

    ppref = purple_plugin_pref_new_with_label("Relays Config:");
    purple_plugin_pref_frame_add(frame, ppref);

    ppref = purple_plugin_pref_new_with_name_and_label("/plugins/core/aa-relays/token", "Token");
    purple_plugin_pref_frame_add(frame, ppref);

    ppref = purple_plugin_pref_new_with_name_and_label("/plugins/core/aa-relays/api_url", "API URL");
    purple_plugin_pref_frame_add(frame, ppref);

    return frame;
}

/* UI */
static PurplePluginUiInfo prefs_info = {
    plugin_config_frame,
    0,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL};

static CURLcode send_post_request_with_json(const char *server, const char *channel, const char *username, const char *content)
{
    CURL *curl;
    CURLcode res;
    const char *token = purple_prefs_get_string("/plugins/core/aa-relays/token");
    purple_debug_info(PLUGIN_ID, "token: %s\n", token);
    const char *api_url = purple_prefs_get_string("/plugins/core/aa-relays/api_url");
    purple_debug_info(PLUGIN_ID, "url: %s\n", api_url);

    if (!token || !api_url || !server || !channel || !username || !content)
    {
        purple_debug_error(PLUGIN_ID, "Invalid inputs.\n");
        return CURLE_FAILED_INIT;
    }

    struct curl_slist *headers = NULL;
    char *api_header = NULL;
    char *json_data = NULL;

    purple_debug_info(PLUGIN_ID, "trying to curl. \n");

    // Initialize curl
    curl = curl_easy_init();
    purple_debug_info(PLUGIN_ID, "curl initialized \n");
    if (!curl)
    {
        purple_debug_error(PLUGIN_ID, "Failed to initialize CURL.\n");
        return CURLE_FAILED_INIT;
    }

    // Construct the API Data
    json_data = g_strdup_printf("{\"server\":\"%s\",\"username\":\"%s\",\"channel\":\"%s\",\"content\":\"%s\"}", server, username, channel, content);
    if (!json_data)
    {
        purple_debug_error(PLUGIN_ID, "Memory allocation for json_data failed.\n");
        curl_easy_cleanup(curl);
        return CURLE_OUT_OF_MEMORY;
    }
    purple_debug_info(PLUGIN_ID, "json built\n");
    // Add the API Key
    api_header = g_strdup_printf("X-API-Key: %s", token);
    if (!api_header)
    {
        purple_debug_error(PLUGIN_ID, "Memory allocation for api_header failed.\n");
        curl_easy_cleanup(curl);
        g_free(json_data);
        return CURLE_OUT_OF_MEMORY;
    }

    // Set the URL
    curl_easy_setopt(curl, CURLOPT_URL, api_url);
    purple_debug_info(PLUGIN_ID, "url set\n");

    // Set headers
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "accept: */*");
    headers = curl_slist_append(headers, api_header);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

    // Specify that this is a POST request
    curl_easy_setopt(curl, CURLOPT_POST, 1L);

    // Set the JSON data as the POST fields
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json_data);

    // Curl timeout, 1second was not enough?
    curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 10000L);

    // Disable SSL Verification, i think my libcurl has no certchain, or access to the OS
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

    // Curl Verbose for Logging
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

    // Perform the request
    res = curl_easy_perform(curl);
    purple_debug_info(PLUGIN_ID, "Curled %d %s\n", res, json_data);
    // purple_debug_info(PLUGIN_ID, "explain %s\n", curl_easy_getinfo(curl, info, ));

    // Check for errors
    if (res != CURLE_OK)
    {
        purple_debug_error(PLUGIN_ID, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    }

    // Cleanup
    curl_easy_cleanup(curl);
    curl_slist_free_all(headers);
    g_free(json_data);
    g_free(api_header);

    return res;
}

static char *get_domain_from_username(const char *input)
// Thanks ChatGPT
{
    if (!input)
    {
        return NULL;
    }

    // Find the position of the '@' character
    const char *at_pos = strchr(input, '@');
    if (!at_pos || *(at_pos + 1) == '\0')
    {
        return NULL;
    }

    // Allocate memory for domain
    size_t domain_len = strlen(at_pos + 1);
    char *domain = (char *)malloc(domain_len + 1);
    if (!domain)
    {
        return NULL;
    }

    // Copy domain part and null-terminate
    strcpy(domain, at_pos + 1);
    return domain;
}

static void relay_received_im_msg(PurpleAccount *account, char *sender, char *message, PurpleConversation *conv, PurpleMessageFlags flags)
{
    // https://docs.imfreedom.org/pidgin2/conversation-signals.html#received-im-msg
    purple_debug_info(PLUGIN_ID, "relay_received_im_msg.\n");
    char *domain = get_domain_from_username(account->username);
    if (domain)
    {
        send_post_request_with_json(
            domain,
            sender,
            sender,
            message);
        free(domain);
    }
}

static void relay_received_chat_msg(PurpleAccount *account, char *sender, char *message, PurpleConversation *conv, PurpleMessageFlags flags)
{
    // https://docs.imfreedom.org/pidgin2/conversation-signals.html#received-chat-msg
    purple_debug_info(PLUGIN_ID, "relay_received_chat_msg.\n");
    char *domain = get_domain_from_username(account->username);
    if (domain)
    {
        send_post_request_with_json(
            domain,
            purple_conversation_get_name(conv),
            sender,
            message);
        free(domain);
    }
}

static gboolean plugin_load(PurplePlugin *plugin)
{
    /* Connect the signal if the plugin is being loaded */
    void *conv_handle = purple_conversations_get_handle();
    purple_signal_connect(conv_handle, "received-im-msg", plugin, PURPLE_CALLBACK(relay_received_im_msg), NULL);
    purple_signal_connect(conv_handle, "received-chat-msg", plugin, PURPLE_CALLBACK(relay_received_chat_msg), NULL);
    return TRUE;
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
    /* Disconnect the signal if the plugin is getting unloaded */
    void *conv_handle = purple_conversations_get_handle();
    purple_signal_disconnect(conv_handle, "received-im-msg", plugin, PURPLE_CALLBACK(relay_received_im_msg));
    purple_signal_disconnect(conv_handle, "received-chat-msg", plugin, PURPLE_CALLBACK(relay_received_chat_msg));
    return TRUE;
}

static void init_plugin(PurplePlugin *plugin)
{
    purple_prefs_add_none("/plugins/core/aa-relays");
    purple_prefs_add_string("/plugins/core/aa-relays/token", "");
    purple_prefs_add_string("/plugins/core/aa-relays/api_url", "");
}

static PurplePluginInfo info = {
    PURPLE_PLUGIN_MAGIC,     /* magic number */
    PURPLE_MAJOR_VERSION,    /* purple major */
    PURPLE_MINOR_VERSION,    /* purple minor */
    PURPLE_PLUGIN_STANDARD,  /* plugin type */
    NULL,                    /* UI requirement */
    0,                       /* flags */
    NULL,                    /* dependencies */
    PURPLE_PRIORITY_DEFAULT, /* priority */

    PLUGIN_ID,                                                 /* id */
    "AA Relays",                                               /* name */
    "0.0.1",                                                   /* version */
    "AA Relays Plugin for Pidgin",                             /* summary */
    "Publishes messages received to an instance of AA Relays", /* description */
    "Joel Falknau <joel.falknau@gmail.com>",                   /* author */
    "https://gitlab.com/tactical-supremacy/aa-relays-pidgin",  /* homepage */

    plugin_load,   /* load */
    plugin_unload, /* unload */
    NULL,          /* destroy */

    NULL,        /* ui info */
    NULL,        /* extra info */
    &prefs_info, /* prefs info */
    NULL,        /* actions */
    NULL,        /* reserved */
    NULL,        /* reserved */
    NULL,        /* reserved */
    NULL         /* reserved */
};

PURPLE_INIT_PLUGIN(AA Relays, init_plugin, info)
