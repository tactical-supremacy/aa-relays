# Pidgin Extension for AA-Relays

## Requires

- libpurple
- glib-2.0
- libcurl

## Compiling

### Linux

install pre-reqs as above
`make build`

### Windows

Hate yourself
The pidgin windev script will take a long ass time to run, this is expected

install cygwin
install this list of requirements with the cygwin manager, dont follow any further <https://pidgin.im/development/building/2.x.y/windows/#cygwin>
install with the cygwin manager `mercurial`, `lynx`, `bsdtar`
open cygwin
`hg clone <https://keep.imfreedom.org/pidgin/windev>`
`cd windev`
`chmod +x pidgin-windev.sh`
`.\pidgin-windev.sh pidgin`
`eval $(./pidgin-windev.sh pidgin --path)`
`cd pidgin/pidgin-2.14.13/pidgin/plugins/`
copy aa-relays.c here
complex build command stored in the Makefile under build-win
never goddamn do this again

need libcurl in root
need aa-relays in the plugin folder

## Using

1. Drag it into your plugin folder
    - Linux
      - `aa-relays.so` - Linux
    - Windows
      - `libcurl.dll` - C:\Program Files (x86)\Pidgin
      - `aa-relays.dll` -  C:\Program Files (x86)\Pidgin\plugins
2. Enable under Pidgin > Plugins
3. Configuration is in Pidgin > Plugins > AA Relays > Configure
    - API_URL = `https://<AUTHURL>/relays/api/submit_jabber_message`
    - API Key = Generate one from Django Ninja API Admin /admin/ninja_apikey/apikey/
4. Ensure your API key is assigned to a user with permission `relays.api_submit`
