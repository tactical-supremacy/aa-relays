import re

from django.conf import settings


def get_site_url():  # regex sso url
    regex = r"^(.+)\/s.+"
    matches = re.finditer(regex, settings.ESI_SSO_CALLBACK_URL, re.MULTILINE)
    url = "http://"

    for m in matches:
        url = m.groups()[0]  # first match

    return url


def aadiscordbot_active():
    return 'aadiscordbot' in settings.INSTALLED_APPS


RELAYS_TRANSLATION_LANGUAGE: str = getattr(settings, "RELAYS_TRANSLATION_LANGUAGE", 'en')
AA_TRANSLATIONS_URL: str = getattr(settings, 'AA_TRANSLATIONS_URL', "http://libretranslate:9095")  # Copy of setting from allianceauth-translation-tool
AA_TRANSLATIONS_API_KEY = getattr(settings,'AA_TRANSLATIONS_API_KEY', None)

RELAYS_TASK_PRIORITY_PROCESSMESSAGE = getattr(settings, "RELAYS_TASK_PRIORITY_PROCESSMESSAGE", 1)
RELAYS_TASK_PRIORITY_SENDMESSAGE = getattr(settings, "RELAYS_TASK_PRIORITY_SENDMESSAGE", 1)
RELAYS_TASK_PRIORITY_PROCESSEMBED = getattr(settings, "RELAYS_TASK_PRIORITY_PROCESSEMBED", 2)
RELAYS_TASK_PRIORITY_SENDEMBED = getattr(settings, "RELAYS_TASK_PRIORITY_SENDEMBED", 2)
RELAYS_APP_BRANDNAME = getattr(settings, "RELAYS_APP_BRANDNAME", "AA Relays")

RELAYS_DOCKERCOMPOSE_DIRECTORY = getattr(
    settings, "RELAYS_DOCKERCOMPOSE_DIRECTORY", "/opt/aa-docker/")
