import logging

from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from relays.docker import destroy_container
from relays.models import (
    AccessToken, Event, Message, MessageEmbed, RunnerContainer,
)
from relays.tasks import process_embed, process_message

from .app_settings import (
    RELAYS_TASK_PRIORITY_PROCESSEMBED, RELAYS_TASK_PRIORITY_PROCESSMESSAGE,
)

logger = logging.getLogger(__name__)


@receiver(post_save, sender=Message)
def message_new(sender, instance: Message, created: bool, *args, **kwargs):
    if created is True:
        process_message.apply_async(
            args=[instance.pk],
            priority=RELAYS_TASK_PRIORITY_PROCESSMESSAGE)
    else:
        return


@receiver(post_save, sender=MessageEmbed)
def embed_new(sender, instance: Message, created: bool, *args, **kwargs):
    if created is True:
        process_embed.apply_async(
            args=[instance.pk],
            priority=RELAYS_TASK_PRIORITY_PROCESSEMBED)
    else:
        return


@receiver(post_save, sender=Event)
def event_new(sender, instance: Event, created: bool, *args, **kwargs):
    # Not Yet Implmented
    pass


@receiver(post_save, sender=AccessToken)
def token_new(sender, instance: AccessToken, created: bool, *args, **kwargs):
    if created is True:
        RunnerContainer.objects.create(token=instance)
    else:
        return


@receiver(pre_delete, sender=AccessToken)
def token_delete(sender, instance: AccessToken, *args, **kwargs):
    runnercontainer = RunnerContainer.objects.get(token=instance)
    destroy_container(runnercontainer)
