from relays.models import Channel, Message, Server, User


def ingest_jabber_message(server, channel, username, content) -> None:
    server_obj = Server.objects.get_or_create(
        domain=server,
        protocol=Server.Protocol_Choice.XMPP,
        defaults={
            "domain": server,
            "name": server,
            "protocol": Server.Protocol_Choice.XMPP})[0]

    channel_obj = Channel.objects.get_or_create(
        server=server_obj,
        name=channel,
        channel_type=Channel.Channel_Type.PIDGIN,
        defaults={
            "name": channel,
            "server": server_obj,
            "channel_type": Channel.Channel_Type.PIDGIN})[0]

    user_obj = User.objects.get_or_create(
        name=username,
        domain=server,
        defaults={
            "name": username,
            "domain": server})[0]

    Message.objects.create(
        channel=channel_obj,
        user=user_obj,
        content=content)
