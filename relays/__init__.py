"""
An Alliance Auth App for forwarding, collating and filtering of messages from various chat services to defined outputs including Database logging.
"""
__version__ = '0.10.2'
__title__ = "AA Relays"
