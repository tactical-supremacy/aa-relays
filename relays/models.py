import logging

from discord import Colour, SyncWebhook
from solo.models import SingletonModel

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


class Relays(models.Model):
    """Meta model for app permissions"""

    class Meta:
        managed = False
        default_permissions = ()
        permissions = (
            ('basic_access', 'Can access this app and submit tokens'),
            ('advanced_access', 'Can access this app and view relay messages'),
            ('api_submit', 'Can submit messages via the API'),
        )


class Server(models.Model):
    """Servers and their ID"""

    class Protocol_Choice(models.TextChoices):
        DISCORD = 'Discord', _('Discord')
        SLACK = 'Slack', _('Slack')
        XMPP = 'XMPP', _('XMPP')

    server = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=256)
    domain = models.CharField(max_length=256, blank=True, null=True, default=None, help_text="This is semi unique for selfhosted services")
    protocol = models.CharField(max_length=10, default=Protocol_Choice.DISCORD, choices=Protocol_Choice.choices)
    users = models.IntegerField(_("Users"), default=0)
    created_at = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
    deleted = models.BooleanField(default=False)
    void = models.BooleanField(_("Ignore and Delete all messages from this server"), default=False)

    # Pre-Calculated Stats

    stat_messages = models.IntegerField(_("Messages"), default=0)
    stat_channels = models.IntegerField(_("Channels"), default=0)

    class Meta:
        verbose_name = _('Server')
        verbose_name_plural = _('Servers')

    def __str__(self) -> str:
        return f"{self.name}"

    @property
    def message_count(self) -> int:
        return self.stat_messages

    @property
    def channel_count(self) -> int:
        return self.stat_channels


class User(models.Model):
    id = models.BigAutoField(_("User ID"), primary_key=True)
    name = models.CharField(_("Username"), max_length=100, blank=True)
    domain = models.CharField(max_length=256, blank=True, null=True, default=None)
    bot = models.BooleanField(_("Is Bot User?"), default=False)
    created_at = models.DateTimeField(
        _("Created Timestamp"), auto_now=False, auto_now_add=False, blank=True, null=True)

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def __str__(self):
        if self.bot is False:
            return f"{self.name}"
        else:
            return f"BOT: {self.name}"


class AccessToken(models.Model):
    """Access Token"""

    token = models.CharField(max_length=256)
    servers = models.ManyToManyField(Server, blank=True)
    appear_offline = models.BooleanField(default=False)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL, blank=True, null=True)

    last_checked = models.DateTimeField(_("Last Checked"), auto_now=False, auto_now_add=False, blank=True, null=True)

    def __str__(self):
        return f"{self.token}"

    class Meta:
        verbose_name = _('Access Token')
        verbose_name_plural = _('Access Tokens')


class RunnerContainer(models.Model):
    """A Containerized Runner"""
    class MethodChoices(models.TextChoices):
        DOCKER_COMPOSE = "docker_compose", _("Default AA Docker Compose")

    token = models.OneToOneField(AccessToken, on_delete=models.CASCADE)
    method = models.CharField(
        _("Containerization Method"),
        max_length=20,
        choices=MethodChoices.choices,
        help_text=_("The Containerization Method to use"),
        default="docker_compose")
    identifier = models.CharField(max_length=256, blank=True, null=True)
    image = models.CharField(max_length=256, blank=True, null=True)
    status = models.CharField(max_length=256, blank=True, null=True)
    health = models.CharField(max_length=256, blank=True, null=True)
    uptime = models.CharField(max_length=256, blank=True, null=True)

    last_checked = models.DateTimeField(_("Last Checked"), auto_now=False, auto_now_add=False, blank=True, null=True)

    def __str__(self):
        return f"{self.token}"

    class Meta:
        verbose_name = _('Runner')
        verbose_name_plural = _('Runners')


class Channel(models.Model):
    """Channel IDs, Names and the Server they belong to"""
    class Channel_Type(models.TextChoices):
        # Thread
        THREAD = 'T', _('Thread')
        # GuildChannel
        TEXTCHANNEL = 'TC', _('GuildChannel.TextChannel')
        FORUMCHANNEL = 'FC', _('GuildChannel.ForumChannel')
        VOICECHANNEL = 'VC', _('GuildChannel.VoiceChannel')
        CATEGORYCHANNEL = 'CC', _('GuildChannel.ForumChannel')
        STAGECHANNEL = 'SC', _('GuildChannel.StageChannel')
        # PrivateChannel0
        DMCHANNEL = 'DM', _('PrivateChannel.DMChannel')
        GROUPCHANNEL = 'GM', _('PrivateChannel.GroupChannel')
        # Pidgin
        PIDGIN = "P", _('Any Pidgin Message, DMs have usernames')

    channel = models.BigAutoField(primary_key=True)
    parent = models.ForeignKey(
        'self', on_delete=models.CASCADE, blank=True, null=True, help_text=_("Channel in Category, Or Thread"), related_name="channel_parent")
    server = models.ForeignKey(Server, on_delete=models.CASCADE, blank=True, null=True)
    recipients = models.ManyToManyField(User, verbose_name=_("Recipients"), blank=True)

    name = models.CharField(max_length=100)
    deleted = models.BooleanField(default=False)
    channel_type = models.CharField(
        max_length=2, choices=Channel_Type.choices, default=Channel_Type.TEXTCHANNEL)
    created_at = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)

    def __str__(self) -> str:
        if self.server is not None:
            return f'{self.name} on {self.server.name}'
        else:
            return f'{self.name}'

    @property
    def channel_as_path(self) -> str:
        if self.parent and self.parent.parent:
            return f"[{self.server}]/{self.parent.parent.name}/{self.parent.name}/{self.name}"
        elif self.parent:
            return f"{self.server}/{self.parent.name}/{self.name}"
        else:
            return f"{self.server}/{self.name}"

    @property
    def channel_as_path_noserver(self) -> str:
        if self.parent and self.parent.parent:
            return f"[{self.parent.parent}/{self.parent}/{self}"
        elif self.parent:
            return f"{self.parent}/{self}"
        else:
            return f"{self}"

    class Meta:
        verbose_name = _('Channel')
        verbose_name_plural = _('Channels')
        indexes = [
            models.Index(fields=["channel_type"]),
        ]


class Message(models.Model):
    """Message Storage"""
    message = models.PositiveBigIntegerField(blank=True, null=True)
    edit_count = models.IntegerField(_("Edit Count"), default=1)

    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    deleted = models.BooleanField(default=False)
    content = models.TextField(help_text=_("The primary content of a message"))
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    mention_everyone = models.BooleanField(_("Message is @everyone"), default=False)

    user = models.ForeignKey(User, verbose_name=_("User"), on_delete=models.CASCADE)
    author_nick = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self) -> str:
        return f'"{self.message}"'

    @property
    def server(self):
        return self.channel.server

    @property
    def content_extended(self) -> str:
        all_content = []
        try:
            for e in self.embeds.all():
                all_content.append(str(e.content))
        except Exception as e:
            logger.exception(e)
        try:
            for a in self.attachments.all():
                all_content.append(str(a.content))
        except Exception as e:
            logger.exception(e)
        content_extended = "\n".join(all_content)

        return content_extended

    class Meta:
        verbose_name = _('Message')
        verbose_name_plural = _('Messages')
        unique_together = ('message', 'edit_count')


class MessageEmbed(models.Model):
    content = models.TextField(_("Message Embed to String"))
    message = models.ForeignKey(Message, verbose_name=_("Message"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("MessageEmbed")
        verbose_name_plural = _("MessageEmbeds")

    def __str__(self) -> str:
        return f'"{self.pk}"'


class MessageAttachment(models.Model):
    id = models.PositiveBigIntegerField(_("Attachment ID"), primary_key=True)
    filename = models.CharField(_("Filename"), max_length=256)
    media_type = models.CharField(_("Media Type"), max_length=50)
    url = models.CharField(_("URL"), max_length=2048)
    message = models.ForeignKey(Message, verbose_name=_("Message"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Message Attachment")
        verbose_name_plural = _("Message Attachments")

    def __str__(self) -> str:
        return f'{self.filename}'


class Webhook(models.Model):
    """Destinations for Relays"""
    url = models.CharField(max_length=2048)
    name = models.CharField(max_length=256)

    def __str__(self) -> str:
        return f"{self.name}"

    class Meta:
        verbose_name = _('Destination Webhook')
        verbose_name_plural = _('Destination Webhooks')

    def send_embed(self, embed):
        webhook = SyncWebhook.from_url(self.url)
        webhook.send(embed=embed, username="AA Relays")


class DestinationAADiscordBot(models.Model):
    """Destination Channels to be passed to AA-Discord
    Bot DON'T set this to hostile channels you potato"""
    class Message_Type(models.TextChoices):
        CHANNEL_MESSAGE = 'CM', _('Channel Message')
        DIRECT_MESSAGE = 'DM', _('Direct Message')

    destination_type = models.CharField(max_length=2,
                                        choices=Message_Type.choices,
                                        default=Message_Type.CHANNEL_MESSAGE)
    destination = models.ForeignKey("aadiscordbot.channels", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.destination}"

    class Meta:
        verbose_name = _('Discord Channel Destination')
        verbose_name_plural = _('Discord Channel Destinations')


class RelayConfiguration(models.Model):
    """In and Out...... Repeat"""
    name = models.CharField(max_length=100)

    source_servers = models.ManyToManyField(Server, blank=True)
    source_server_all = models.BooleanField(default=False)
    source_channels = models.ManyToManyField(Channel, blank=True)
    source_channel_all = models.BooleanField(default=False)

    message_mention = models.BooleanField(default=True)
    message_non_mention = models.BooleanField(default=False)
    message_regex = models.CharField(max_length=10, default=".^", blank=False, null=False)

    relay_event = models.BooleanField(default=True)
    relay_embed = models.BooleanField(default=True)
    relay_private_message = models.BooleanField(default=True, help_text=_("Not Yet Implemented"))

    destination_webhooks = models.ManyToManyField(Webhook, blank=True)
    destination_aadiscordbot = models.ManyToManyField(DestinationAADiscordBot, blank=True)

    attempt_translation = models.BooleanField(default=False)

    class Colours(models.TextChoices):
        BLUE = str(Colour.blue()), _('Blue')
        BLURPLE = str(Colour.blurple()), _('blurple')
        BRAND_GREEN = str(Colour.brand_green()), _('brand_green')
        BRAND_RED = str(Colour.brand_red()), _('brand_red')
        DARK_BLUE = str(Colour.dark_blue()), _('dark_blue')
        DARK_GOLD = str(Colour.dark_gold()), _('dark_gold')
        DARK_GREY = str(Colour.dark_grey()), _('dark_grey')
        DARK_GREEN = str(Colour.dark_green()), _('dark_green')
        DARK_MAGENTA = str(Colour.dark_magenta()), _('dark_magenta')
        DARK_ORANGE = str(Colour.dark_orange()), _('dark_orange')
        DARK_PURPLE = str(Colour.dark_purple()), _('dark_purple')
        DARK_RED = str(Colour.dark_red()), _('dark_red')
        DARK_TEAL = str(Colour.dark_teal()), _('dark_teal')
        DARK_THEME = str(Colour.dark_theme()), _('dark_theme')
        DARKER_GREY = str(Colour.darker_grey()), _('darker_grey')
        EMBED_BACKGROUND = str(Colour.embed_background('dark')), _('embed_background')
        FUCHSIA = str(Colour.fuchsia()), _('fuchsia')
        GOLD = str(Colour.gold()), _('gold')
        GREEN = str(Colour.green()), _('green')
        GREYPLE = str(Colour.greyple()), _('greyple')
        LIGHT_GREY = str(Colour.light_grey()), _('light_grey')
        LIGHTER_GREY = str(Colour.lighter_grey()), _('lighter_grey')
        NITRO_PINK = str(Colour.nitro_pink()), _('nitro_pink')
        OG_BLURPLE = str(Colour.og_blurple()), _('og_blurple')
        ORANGE = str(Colour.orange()), _('orange')
        PURPLE = str(Colour.purple()), _('purple')
        RED = str(Colour.red()), _('red')
        TEAL = str(Colour.teal()), _('teal')
        YELLOW = str(Colour.yellow()), _('yellow')

    override_colour = models.CharField(_("Override Colour"), max_length=50, choices=Colours.choices, blank=True)

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = _('Relay Configuration')
        verbose_name_plural = _('Relay Configurations')


class Event(models.Model):
    event = models.PositiveBigIntegerField(primary_key=True)
    name = models.CharField(_("Name"), max_length=100)
    description = models.TextField(_("Description"))
    start = models.DateTimeField(auto_now=False, auto_now_add=False)
    end = models.DateTimeField(auto_now=False, auto_now_add=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    deleted = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("ScheduledEvent")
        verbose_name_plural = _("ScheduledEvents")

    def __str__(self):
        return f"{self.name}"


class RelaysConfig(SingletonModel):
    status_webhooks = models.ManyToManyField(
        Webhook, verbose_name=_("Destination Webhook for Status Updates"))

    def __str__(self):
        return f"{self.pk}"

    class Meta:
        """
        Meta definitions
        """
        default_permissions = ()
        verbose_name = _("AA Relays Settings")
        verbose_name_plural = _("AA Relays Settings")


datatables_dict = {
    # Model and mapped fields to pull in API
    'message': ['id', 'channel', 'content'],
    'server': ['name', 'protocol', 'users'],
}


datatables_class_dict = {
    # Map names to Model's
    'message': 'Message',
    'server': 'Server',
}
