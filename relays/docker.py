import os

import docker
from docker.errors import NotFound
from docker.types import LogConfig, RestartPolicy

from django.conf import settings
from django.utils import timezone

from allianceauth.services.hooks import get_extension_logger

from relays.models import AccessToken, RunnerContainer

logger = get_extension_logger(__name__)


def _get_container(token: AccessToken):
    client = docker.from_env()
    return client.containers.get(f"allianceauth_relay_{token.pk}")


def ping_container(runnercontainer: RunnerContainer) -> None:
    try:
        container = _get_container(runnercontainer.token)
        container.reload()
        runnercontainer.identifier = container.id
        runnercontainer.image = container.image
        runnercontainer.status = container.status
        runnercontainer.health = container.health
        runnercontainer.last_checked = timezone.now()
        runnercontainer.save()
    except NotFound:
        runnercontainer.identifier = ""
        runnercontainer.image = ""
        runnercontainer.status = "Not Found"
        runnercontainer.health = ""
        runnercontainer.last_checked = timezone.now()
        runnercontainer.save()
    except Exception as e:
        logger.exception(e)


def start_container(runnercontainer: RunnerContainer):
    try:
        _get_container(runnercontainer.token).start()
        ping_container(runnercontainer)
    except NotFound:
        client = docker.from_env()
        return client.containers.run(
            name=f"allianceauth_relay_{runnercontainer.token.pk}",
            image=os.environ.get("AA_DOCKER_TAG"),
            entrypoint=[
                "python",
                "/home/allianceauth/myauth/runner_discord.py",
                f"{runnercontainer.token.pk}"],
            working_dir="/home/allianceauth/myauth/",
            init=True,
            environment={
                "PROTOCOL": os.environ.get("PROTOCOL"),
                "AUTH_SUBDOMAIN": os.environ.get("AUTH_SUBDOMAIN"),
                "DOMAIN": os.environ.get("DOMAIN"),
                "AA_DOCKER_TAG": os.environ.get("AA_DOCKER_TAG"),
                "AA_SITENAME": os.environ.get("AA_SITENAME"),
                "AA_SECRET_KEY": os.environ.get("AA_SECRET_KEY"),
                "AA_DB_HOST": os.environ.get("AA_DB_HOST"),
                "AA_DB_NAME": os.environ.get("AA_DB_NAME"),
                "AA_DB_USER": os.environ.get("AA_DB_USER"),
                "AA_DB_PASSWORD": os.environ.get("AA_DB_PASSWORD"),
                "AA_DB_ROOT_PASSWORD": os.environ.get("AA_DB_ROOT_PASSWORD"),
                "ESI_SSO_CLIENT_ID": os.environ.get("ESI_SSO_CLIENT_ID"),
                "ESI_SSO_CLIENT_SECRET": os.environ.get("ESI_SSO_CLIENT_SECRET"),
                "ESI_USER_CONTACT_EMAIL": os.environ.get("ESI_USER_CONTACT_EMAIL"),
                "DISCORD_GUILD_ID": os.environ.get("DISCORD_GUILD_ID"),
                "DISCORD_CALLBACK_URL": os.environ.get("DISCORD_CALLBACK_URL"),
                "DISCORD_APP_ID": os.environ.get("DISCORD_APP_ID"),
                "DISCORD_APP_SECRET": os.environ.get("DISCORD_APP_SECRET"),
                "DISCORD_BOT_TOKEN": os.environ.get("DISCORD_BOT_TOKEN")
            },
            restart_policy=RestartPolicy("on-failure", max_attempts=1),
            log_config=LogConfig(
                type=LogConfig.types.JSON,
                config={'max-size': '5m'}),
            healthcheck={
                'test': ['CMD', '/memory_check.sh', '500000000'],
                'interval': 6000000000,
                'timeout': 6000000000,
                'retries': 3,
                'start_period': 300000000000
            },
            volumes=[
                f"{settings.RELAYS_DOCKERCOMPOSE_DIRECTORY}conf/local.py:/home/allianceauth/myauth/myauth/settings/local.py",
                f"{settings.RELAYS_DOCKERCOMPOSE_DIRECTORY}conf/celery.py:/home/allianceauth/myauth/myauth/celery.py",
                f"{settings.RELAYS_DOCKERCOMPOSE_DIRECTORY}conf/urls.py:/home/allianceauth/myauth/myauth/urls.py",
                f"{settings.RELAYS_DOCKERCOMPOSE_DIRECTORY}conf/runner_discord.py:/home/allianceauth/myauth/runner_discord.py",
                f"{settings.RELAYS_DOCKERCOMPOSE_DIRECTORY}conf/memory_check.sh:/memory_check.sh"
            ],
            network_mode="container:allianceauth_gunicorn",
            detach=True,
        )


def stop_container(runnercontainer: RunnerContainer) -> None:
    try:
        container = _get_container(runnercontainer.token)
        container.stop()
        ping_container(runnercontainer)
    except Exception as e:
        logger.exception(e)


def destroy_container(runnercontainer: RunnerContainer) -> None:
    try:
        stop_container(runnercontainer)
        client = docker.from_env()
        client.containers.prune()
    except Exception as e:
        logger.exception(e)


def restart_container(runnercontainer: RunnerContainer) -> None:
    try:
        container = _get_container(runnercontainer.token)
        container.restart()
        ping_container(runnercontainer)
    except NotFound:
        start_container(runnercontainer)
        ping_container(runnercontainer)
    except Exception as e:
        logger.exception(e)
