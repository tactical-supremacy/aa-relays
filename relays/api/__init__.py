from ninja import NinjaAPI
from ninja.security import django_auth

from django.conf import settings

from allianceauth.services.hooks import get_extension_logger

from relays.api.datatables import DatatablesApiEndpoints
from relays.api.jabber import JabberApiEndpoints

logger = get_extension_logger(__name__)

api = NinjaAPI(
    title="Relays API",
    version="0.0.1",
    auth=django_auth,
    csrf=True,
    openapi_url=settings.DEBUG and "/openapi.json" or "")


def setup(api) -> None:
    JabberApiEndpoints(api)
    DatatablesApiEndpoints(api)


setup(api)
