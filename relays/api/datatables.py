import importlib

from ninja import NinjaAPI, Query, Schema

from django.db.models import Q

from relays.models import Message, datatables_class_dict, datatables_dict


class Datatables(Schema):
    length: int = 100
    start: int = 0
    draw: int = 0
    search: dict = {"search[value]": ""}
    order: list[dict] = [{"0": {"column": 0}}, {"0": {"dir": "desc"}}]
# order[0][column]=3&order[0][dir]=asc
# search[value]=test


class DatatablesServerMessages(Schema):
    length: int = 100
    start: int = 0
    draw: int = 0
    search: dict = {"search[value]": ""}
    order: list[dict] = [{"0": {"column": 0}}, {"0": {"dir": "desc"}}]
# order[0][column]=3&order[0][dir]=asc
# search[value]=test


class DatatablesApiEndpoints:
    tags = ["Datatables"]

    def __init__(self, api: NinjaAPI) -> None:

        @api.get("datatables/{source_model}", tags=["Datatables"])
        def datatables(request, source_model, params: Query[Datatables]):
            # BEGIN ServerSide Datatables
            length = int(params.length)
            start = int(params.start)
            limit = start + length
            search_string = request.GET['search[value]']  # The dict wont cast, use manual GET
            try:
                order_col = int(request.GET['order[0][column]'])  # The dict wont cast, use manual GET
            except Exception:
                order_col = 0
            order_dir = request.GET['order[0][dir]']  # The dict wont cast, use manual GE
            draw = int(params.draw)
            # END ServerSide Datatables

            # Specifically these two dynamically fetch columns from a model def
            columns = datatables_dict[source_model]
            mod_ref = datatables_class_dict[source_model]

            filter_q = Q()
            if len(search_string) > 0:
                val = search_string
                for x in columns:
                    filter_q |= Q(**{f'{x}__icontains': val})  # Apply search to all Columns

            order = columns[order_col]
            if order_dir == 'desc':
                order = '-' + order

            module = importlib.import_module('relays.models')
            mod_class = getattr(module, mod_ref)

            data = mod_class.objects.filter(filter_q).order_by(order).values_list(*columns)[start:limit]

            item_count = mod_class.objects.filter(filter_q).count()
            json_data = list(data)

            datatables_data = {}
            datatables_data['draw'] = draw
            datatables_data['recordsTotal'] = item_count
            datatables_data['recordsFiltered'] = item_count
            datatables_data['data'] = json_data

            return datatables_data

        @api.get("datatables_server_messages/{server_id}", tags=["Datatables", "Server"])
        def datatables_server_messages(request, server_id, params: Query[DatatablesServerMessages]):
            # BEGIN ServerSide Datatables _template_
            length = int(params.length)
            start = int(params.start)
            limit = start + length
            search_string = request.GET['search[value]']  # The dict wont cast, use manual GET
            try:
                order_col = int(request.GET['order[0][column]'])  # The dict wont cast, use manual GET
            except Exception:
                order_col = 0
            order_dir = request.GET['order[0][dir]']  # The dict wont cast, use manual GET
            draw = int(params.draw)
            # END ServerSide Datatables _template_

            # defining my own
            columns = ["timestamp", "channel__name", "author_nick", "content"]

            filter_q = Q()
            if len(search_string) > 0:
                val = search_string
                for x in columns:
                    filter_q |= Q(**{f'{x}__icontains': val})  # Apply search to all Columns

            order = columns[order_col]
            if order_dir == 'desc':
                order = '-' + order

            datatables_data = {}
            datatables_data['draw'] = draw
            datatables_data['recordsTotal'] = Message.objects.filter(channel__server_id=server_id).count()
            datatables_data['recordsFiltered'] = Message.objects.filter(channel__server_id=server_id).filter(filter_q).count()
            datatables_data['data'] = list(Message.objects.filter(channel__server_id=server_id).filter(filter_q).order_by(order).values_list(*columns)[start:limit])

            return datatables_data
