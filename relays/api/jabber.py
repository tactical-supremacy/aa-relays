from ninja import NinjaAPI, Schema
from ninja_apikey.security import APIKeyAuth

from django.http import HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

from relays.ingest_jabber import ingest_jabber_message

api_key_auth = APIKeyAuth()


class Message(Schema):
    server: str
    channel: str
    username: str
    content: str


class JabberApiEndpoints:
    tags = ["Jabber", "Submit"]

    def __init__(self, api: NinjaAPI) -> None:

        @api.post("/submit_jabber_message", auth=api_key_auth)
        @csrf_exempt
        def submit_jabber_message(request, message: Message):
            if not request.user.has_perm("relay.api_submit"):
                return HttpResponseForbidden("You do not have permission to submit messages.")
            try:
                ingest_jabber_message(message.server, message.channel, message.username, message.content)
                return {"success": True}
            except Exception as e:
                # Log the error and return a 500 response
                return {"success": False, "error": str(e)}
