import logging

from django.contrib import admin
from django.contrib.admin.widgets import AutocompleteSelectMultiple
from django.forms import ModelMultipleChoiceField

from relays.docker import (
    destroy_container, ping_container, restart_container, start_container,
    stop_container,
)
from relays.models import (
    AccessToken, Channel, DestinationAADiscordBot, Event, Message,
    RelayConfiguration, RelaysConfig, RunnerContainer, Server, Webhook,
)

logger = logging.getLogger(__name__)


@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
    list_display = ["name", "users", "protocol", "deleted", "server", "void"]
    list_filter = ["protocol", "deleted", "void"]
    ordering = ["-users", ]
    search_fields = ["name", "protocol"]


@admin.register(AccessToken)
class AccessTokenAdmin(admin.ModelAdmin):
    list_display = ["token", "_server_list", "owner"]
    ordering = ["owner", ]
    list_filter = ['servers', 'owner', ]
    actions = ['generate_runner_config']

    def _server_list(self, obj):
        return "\n".join([server.name for server in obj.servers.all()])

    @admin.action(description="Create Containerized Runner Configurations")
    def generate_runner_config(modeladmin, request, queryset):
        for token in queryset:
            try:
                RunnerContainer.objects.create(token=token)
            except Exception:
                pass


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    list_display = ['name', 'parent', 'server',
                    "deleted", "channel_type", 'channel']
    ordering = ['name', ]
    list_filter = ['server', "channel_type", "deleted"]
    search_fields = ['name', 'parent', 'server', ]


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ["message", "channel", "_servername",
                    "author_nick", "timestamp", "edit_count"]
    ordering = ['-timestamp',]
    list_filter = ["channel", "deleted"]
    search_fields = ["content", "channel", "server"]

    def _servername(self, obj):
        try:
            return obj.channel.server.name
        except Exception:
            return ""


@admin.register(Webhook)
class WebhookAdmin(admin.ModelAdmin):
    list_display = ["name", "url"]
    ordering = ["name", ]
    search_fields = ["name", ]


@admin.register(DestinationAADiscordBot)
class DestinationAADiscordBotAdmin(admin.ModelAdmin):
    list_display = ["destination", "destination"]
    ordering = ["destination", ]

    search_fields = ["destination", ]


class SourceChannelField(AutocompleteSelectMultiple):
    def label_from_instance(self, obj):
        return f"{obj} - {obj.server}"


@admin.register(RelayConfiguration)
class RelayConfigurationAdmin(admin.ModelAdmin):
    list_display = ["name", "message_mention", "message_non_mention",
                    "message_regex", "_source_server_list", "_source_channel_list", ]
    ordering = ["name", ]
    search_fields = ["name", ]

    filter_horizontal = [
        'source_servers', 'source_channels', 'destination_webhooks', 'destination_aadiscordbot']

    def _source_server_list(self, obj: RelayConfiguration):
        if obj.source_server_all is True:
            return "ALL"
        else:
            return "\n".join([source_server.name for source_server in obj.source_servers.all()])

    def _source_channel_list(self, obj: RelayConfiguration):
        if obj.source_channel_all is True:
            return "ALL"
        else:
            return "\n".join([source_channel.name for source_channel in obj.source_channels.all()])

    def formfield_for_manytomany(self, db_field, request, **kwargs) -> ModelMultipleChoiceField:
        # Filter out Void/Ignored Servers and Channels
        if db_field.name == "source_servers":
            kwargs["queryset"] = Server.objects.filter(void=False, deleted=False)
        if db_field.name == "source_channels":
            kwargs["queryset"] = Channel.objects.filter(server__void=False, deleted=False)
        return super().formfield_for_manytomany(db_field, request, **kwargs)


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ["name", ]
    ordering = ["start", ]
    search_fields = ["name", ]


@admin.register(RelaysConfig)
class RelaysConfigAdmin(admin.ModelAdmin):
    filter_horizontal = ["status_webhooks", ]


@admin.register(RunnerContainer)
class RunnerContainerAdmin(admin.ModelAdmin):
    list_display = ["token", "status", "health", "image", "identifier", "method", "uptime", "last_checked", ]
    list_filter = ["status", "health", "image", "method"]
    actions = ['ping_container', 'start_container', 'restart_container', 'stop_container', 'destroy_container', ]

    @admin.action(description="Update the Status of Selected Runners")
    def ping_container(modeladmin, request, queryset):
        for runner in queryset:
            ping_container(runner)

    @admin.action(description="Start Selected Runners")
    def start_container(modeladmin, request, queryset):
        for runner in queryset:
            start_container(runner)

    @admin.action(description="Restart Selected Runners")
    def restart_container(modeladmin, request, queryset):
        for runner in queryset:
            restart_container(runner)

    @admin.action(description="Stop Selected Runners")
    def stop_container(modeladmin, request, queryset):
        for runner in queryset:
            stop_container(runner)

    @admin.action(description="Destroy (Stop and Prune) Runners")
    def destroy_container(modeladmin, request, queryset):
        for runner in queryset:
            destroy_container(runner)
