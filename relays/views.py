from django.contrib.auth.decorators import login_required, permission_required
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponse
from django.shortcuts import render

from relays.models import Server


@login_required
@permission_required('relays.basic_access')
def index(request) -> HttpResponse:

    context = {
        "servers": Server.objects.filter(void=False).order_by("-users")
    }

    return render(request, 'relays/index-bs5.html', context)


@login_required
@permission_required("relays.basic_access")
def server(request: WSGIRequest, server: int) -> HttpResponse:
    """
    A relayed Server
    :param request:
    :return:
    """
    context = {}
    context['server'] = Server.objects.get(server=server)

    return render(request, "relays/server-bs5.html", context)
